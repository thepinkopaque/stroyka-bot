# stroyka-bot

Общая концепция:
Телеграм-бот написанный на языке Rust, который может следующее: 
- Выдавать приветствие/прощание 
- Кто является автором (ФИО, группа, подгруппа)
- На каком направлении мы обучается
- Каковы особенности данного направления (Что дает это направление, кем мы станем, список дисциплин)
Бот реагирует на команды и сообщения, содержащие в себе какой-то вопрос
Принцип работы:  бот находится на виртуальной машине в облаке, предоставленной [Cloud.ru](https://console.cloud.ru/)
. Бот запущен внутри докер-крнтейнера - это нужно для предсказуемости работы. Код бота находится в публичном репозитории (для возможности демонстрации кода) на Gitlab. Также настроен CI/CD - при изменении в коде обновляется и сам бот. Для написания бота выбран язык Rust, библиотека - [Teloxide](https://github.com/teloxide/teloxide).

Как воспроизвести и подправить бота: 

```
# клонируем репозиторий на рабочий компьютер
git clone https://gitlab.com/thepinkopaque/stroyka-bot.git
cd stroyka-bot
# собираем первичный образ
docker build . -t mybot-rust0 -f Dockerfile.init

# далее по изменению кода, прогоняем пересборку образа
docker build . -t mybot-rust1 ; docker compose up -d


```

--------
Для начала работы с ботом, найдите в Телеграме [@stroyka_KVKPOBot](https://t.me/stroyka_KVKPOBot) и нажмите /start, далее выбираете интересующий вопрос в меню, либо самостоятельно пишете ему вопрос.

В идеале картина должна была быть такой, но так как ситуация складыватся, как складывается, то гитлаб не позволил нам запускать пайплайны (набор действий, которые должен делать гитлаб-раннер при изменении кода, но это невозможно без верификации аккаунта, которая на данный момент на территории РФ невозможна ;) )

```mermaid
flowchart TB
    id1([Пользователь])-->|взаимодействует с ботом|id2(Телеграм)
    id2(Телеграм)<-->|работа по API|a
    id3{{Разработчик}}-->id4[/Gitlab/]
    id3{{Разработчик}}-->|ssh <юзер>@<сервер>|Виртуалка
    id4[/Gitlab/]-->|триггерит|i1
    subgraph Cloud.ru
    direction TB
    subgraph Виртуалка
        direction RL
        i1(Gitlab Runner) -->|пересобирает образ контейнера|Докер
        subgraph Докер
    
            direction TB
            a(Контейнер с ботом)
        end
        end
end
```