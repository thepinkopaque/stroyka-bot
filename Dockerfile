FROM mybot-rust0

WORKDIR /usr/src/bot
COPY . .

RUN cargo install --path .

CMD ["stroyka-bot"]